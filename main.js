document.querySelector('#shootButton').onclick = function () {
    let userChoice = document.getElementById('userInput').value

    let compChoice = Math.floor((Math.random() * 3))

    if (compChoice == 1) {
        compChoice = 'rock'
    } else if (compChoice == 2) {
        compChoice = 'paper'
    } else {
        compChoice = 'scissors'
    }

    function compare(choice1, choice2) {
        if (userChoice !== 'rock' && userChoice !== 'paper' && userChoice !== 'scissors') {
            alert ('That is cheating! Pick a valid weapon!')
        }

        if (choice1 === choice2) {
            alert ('You have tied the machine. Try again')
        }

        if (choice1 === 'rock') {
            if (choice2 === 'scissors') {
                alert ('Congratulations, you have defeated the machine!')
            } else {
                alert ('The machine wins...')
            }
        }

        if (choice1 === 'paper') {
            if (choice2 === 'rock') {
                alert ('Congratulations, you have defeated the machine!')
            } else {
                alert ('The machine wins...')
            }
        }

        if (choice1 === 'scissors') {
            if (choice2 === 'rock') {
                alert ('The machine wins...')
            } else {
                alert ('Congratulations, you have defeated the machine!')
            }
        }
    }

    compare(userChoice, compChoice)
}